class MemoryException(Exception):
    pass


class BroadcastingException(Exception):
    pass


class MNDIndexException(Exception):
    pass


class KernelException(Exception):
    pass


class ContextManagerException(Exception):
    pass


class SequenceAnalysisUnitException(Exception):
    pass


class KernelMapException(Exception):
    pass


class EmbedderException(Exception):
    pass
