from typing import Dict, Type, Callable, Any, Iterable, Optional

import numpy as np
import torch
from src.Exceptions import BroadcastingException


class Broadcast:
    def __init__(self,
                 map: Dict[Type, Callable[[Any], Any]] = {
                     np.ndarray: torch.from_numpy,
                     list: lambda x: torch.Tensor(x),
                     torch.Tensor: lambda t: t
                 },
                 inverse_map: Optional[Dict[Type, Callable[[Any], Any]]] = {
                     np.ndarray: lambda t: t.tolist(),
                     list: lambda t: t,
                     torch.Tensor: lambda t: t.tolist()
                 },
                 to_type: Optional[Type] = torch.Tensor):
        if not isinstance(map, dict):
            raise BroadcastingException(f"Map expected dict, got {type(map)}")
        if inverse_map:
            if not isinstance(inverse_map, dict):
                raise BroadcastingException(f"Inverse map expected dict, got {type(inverse_map)}")
        self.map = map
        self.inverse_map = inverse_map
        self.from_types = map.keys()
        self.to_type = to_type

    def __call__(self, x, *args, **kwargs):
        if type(x) not in self.from_types:
            raise BroadcastingException(f"Passed object of type {type(x)} is not among the supported types {list(self.from_types)}")
        try:
            return self.map[type(x)](x)
        except Exception:
            raise BroadcastingException(f"Couldn't broadcast the passed object")

    def __iter__(self):
        for type in self.from_types:
            yield type

    def inverse(self, x):
        """
        Uses the inverse broadcasting map if it is initialized
        :param x: object to broadcast
        """
        if self.inverse_map:
            if type(x) not in self.from_types:
                raise BroadcastingException(
                    f"Passed object of type {type(x)} is not among the supported types {list(self.from_types)}")
            try:
                return self.inverse_map[type(x)](x)
            except Exception:
                raise BroadcastingException(f"Couldn't broadcast the passed object")
