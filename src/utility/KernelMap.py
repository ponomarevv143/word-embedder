import os
from typing import Optional, Dict, Union, List

import torch

from src.Exceptions import *
from src.utility import MNDIndex
import pickle, json


class KernelMap:
    """
    Acts as an intermediary between the MNDIndex and the Kernel representations.
    Given an MNDIndex M: T_w <-> I_w and the raw Kernel index K: w -> T_w,
    the kernel map KM provides an interface KM: I_w <-> w by composing maps M and K
    """

    def __init__(self,
                 embedding_dimension: int,
                 n_cells: Optional[int] = 3,
                 n_probes: Optional[int] = 3,
                 training_subset_length: int = 150,
                 retraining_frequency: int = 1e4):
        """
        :param embedding_dimension: The embedding dimension for the kernel.
        :param n_cells: Number of Voronoi cells for the Faiss index.
        :param n_probes: Number of probes for the Faiss index.
        :param training_subset_length: A positive integer, defining how many words must be encountered before
        training the Faiss index.
        :param retraining_frequency: A sufficiently big positive integer that shows the number
        of operations of adding/updating the index values that need to happen
        before the index is retrained. (if 0 or less, does not retrain)
        """
        try:
            # TODO: Retraining using the factory for continuous recreation after fixed number of steps.
            self.__mnd_index_factory = \
                lambda: MNDIndex(size=[embedding_dimension, embedding_dimension],
                                 n_cells=n_cells,
                                 n_probes=n_probes)
            self.mnd_index = self.__mnd_index_factory()
        except MNDIndexException as e:
            raise KernelMapException("Error has occurred during the initialization of the MNDIndex: ", e)

        self.embedding_dimension = embedding_dimension
        self.direct_map: Dict[str, int] = dict()
        self.inverse_map: Dict[int, str] = dict()
        self.__training_map: Dict[str, torch.Tensor] = dict()
        self.__training_subset_length = training_subset_length
        # If equal to retraining_frequency, retrain the index.
        self.__operation_counter = 0
        self.retraining_frequency = retraining_frequency
        self.__trained = False

    def __train(self) -> None:
        # Trains the index by migrating the training map to the actual index
        if not self.__trained:
            # If conditions are satisfied
            if len(self.__training_map) >= self.__training_subset_length:
                # Build the index;
                self.mnd_index.build(list(self.__training_map.values()))
                # Construct the maps;
                for word, representation in self.__training_map.items():
                    # Get the idx
                    idx = self.mnd_index.get_neighbors_indices(representation)[0]
                    self.direct_map[word] = idx
                    self.inverse_map[idx] = word
                # Free the training map and set trained to True
                del self.__training_map
                self.__trained = True

    def reconstruct(self, idx: int) -> torch.Tensor:
        # Facade for MNDIndex reconstruct
        return self.mnd_index.reconstruct(idx)

    def __retrain(self) -> None:
        """
        Retrains the index.
        """
        # Get the saved data.
        word_rep_map: Dict[str, torch.Tensor] = {word: self.reconstruct(idx) for word, idx in self.direct_map.items()}
        # Construct new index
        self.mnd_index = self.__mnd_index_factory()
        # Train it based off the previous values
        self.mnd_index.add(list(word_rep_map.values()))
        # Remake the direct and inverse maps
        for word in word_rep_map.keys():
            idx = self.mnd_index.get_neighbors_indices(word_rep_map[word])[0]
            self.direct_map[word] = idx
            self.inverse_map[idx] = word

    def get(self, word: str) -> torch.Tensor:
        """
        Returns the tensor representation of the word.
        :param word: Query.
        :return: Tensor representation of the query.
        :raises KernelMapException: Word is not found in the map.
        """
        try:
            # If trained, use the direct map and reconstruction.
            if self.__trained:
                return self.reconstruct(self.direct_map[word])
            # Else, use the training map.
            else:
                return self.__training_map[word]
        except KeyError:
            raise KernelMapException(f"Couldn't find {word} in the map.")

    def get_closest_neighbors(self,
                              query: torch.Tensor,
                              k: int = 1) -> Union[List[torch.Tensor]]:
        try:
            neighbors = self.mnd_index.get_neighbors(query, k)
        except MNDIndexException as e:
            raise KernelMapException(f"Error has occurred during reverse search: {e}")
        return neighbors

    def search(self,
               representation: Union[torch.Tensor, List[torch.Tensor]]) -> Union[str, List[str]]:
        """
        Performs an inverse search, returning the word closest to the passed representation.
        :param representation: Tensor representation.
        :return: Closest word found.
        """
        if type(representation) is list:
            return [self.search(rep) for rep in representation]
        if self.__trained:
            idx = self.mnd_index.get_neighbors_indices(representation)[0]
            return self.inverse_map[idx]
        else:
            # Bruteforce - find the minimum pair (word, representation) in the training map w.r.t the norm,
            # return the word.
            return min([(word, T - representation) for word, T in self.__training_map.items()],
                       key=lambda pair: torch.norm(pair[1]))[0]

    def search_closest_neighbors(self,
                                 query: torch.Tensor,
                                 k: int = 1) -> List[str]:
        """
        Finds string representations for each of the neighbor of the query.
        """
        # If trained, pass indices of closest neighbors to the direct map.
        if self.__trained:
            neighbor_indices = self.mnd_index.get_neighbors_indices(query, k)
            return [self.inverse_map[idx] for idx in neighbor_indices]
        # If not trained, bruteforce by sorting the list of (word, rep) pairs in the ascending
        # order, defined by the proximity to the original query using an L2 norm,
        # then extracting the words and getting first K (i.e. K closest words fitting the rep)
        else:
            return [word for word, T in sorted([(word, T - query) for word, T in self.__training_map.items()],
                                               key=lambda t1, t2: t1 - t2)][:k]

    def set(self,
            word: Union[str, List[str]],
            representation: Union[torch.Tensor, List[torch.Tensor]]) -> None:
        """
        Adds word(s) and their representation(s) to the map.
        :param word: A word or a list of words.
        :param representation: A representation or a list of representations for each passed word.
        """
        # Wrapper for List case
        if type(word) is list:
            if type(representation) is not list:
                raise KernelMapException(f"Expected passed representations to be a list, "
                                         f"received {type(representation)}")
            if len(representation) != len(word):
                raise KernelMapException(f"Length mismatch: received a list of words of length {len(word)} "
                                         f"and a list of representations of length {len(representation)}.")
            for w, T in zip(word, representation):
                self.set(w, T)

        else:
            # If trained:
            if self.__trained:
                # If the word is already learned, update
                if word in self.direct_map:
                    self.mnd_index.update(
                        # Old value is stored in the index,
                        # New value is the passed representation.
                        (self.get(word), representation)
                    )
                    self.__operation_counter += 1
                # Otherwise, add the representation and index the word in both maps.
                else:
                    # 1) Add the representation
                    self.mnd_index.add([representation])
                    # 2) Get the idx of the representation, add (word, idx) and (idx, word) to respective maps.
                    idx = self.mnd_index.get_neighbors_indices(representation)[0]
                    self.direct_map[word] = idx
                    self.inverse_map[idx] = word
            # If not trained:
            else:
                # Just add the word-rep pair to the training map.
                self.__training_map[word] = representation

            if not self.__trained:
                if len(self.__training_map) > self.__training_subset_length:
                    self.__train()
            else:
                if self.retraining_frequency > 0:
                    # Retrain the index using the values saved in the previous index.
                    if self.__operation_counter >= self.retraining_frequency:
                        self.__retrain()

    def key_exists(self, word: str) -> bool:
        """
        Method to check whether the kernel map has passed word in the key set.
        :param word: Key to check.
        :return: True if key is in the key set, False otherwise 
        """
        if self.__trained:
            return word in self.direct_map
        else:
            return word in self.__training_map

    def save(self,
             directory: str,
             conf_file: str = "kernel_map.json") -> None:

        if not os.path.exists(directory):
            os.mkdir(directory)

        # Save index
        self.mnd_index.save(directory + "mndindex/")

        # Save maps
        with open(directory + "direct_map.dat", "wb") as out:
            pickle.dump(self.direct_map, out)

        with open(directory + "inverse_map.dat", "wb") as out:
            pickle.dump(self.inverse_map, out)

        if not self.__trained:
            with open(directory + "training_map.dat", "wb") as out:
                pickle.dump(self.__training_map, out)

        # Save config
        with open(directory + conf_file, "w") as out:
            out.write(json.dumps(
                {
                    "embedding_dimension": self.embedding_dimension,
                    "direct_map_file": directory + "direct_map.dat",
                    "inverse_map_file": directory + "inverse_map.dat",
                    "retraining_frequency": self.retraining_frequency,
                    "mnd_index_conf_file": directory + "mndindex/" + "mnd_index.json",
                    "trained": self.__trained,
                    "training_map_file": False if self.__trained is None else directory + "training_map.dat"
                }
            ))

    @staticmethod
    def load(conf_file: str):
        # Load config
        with open(conf_file, "r") as inp:
            config = json.loads(inp.read())

        # Load maps
        with open(config["direct_map_file"], "rb") as inp:
            direct_map = pickle.load(inp)

        with open(config["inverse_map_file"], "rb") as inp:
            inverse_map = pickle.load(inp)

        # Load index, create new map
        mnd_index = MNDIndex.load(config["mnd_index_conf_file"])
        kernel_map = KernelMap(embedding_dimension=int(config["embedding_dimension"]),
                               retraining_frequency=int(config["retraining_frequency"]))

        if not config["trained"]:
            with open(config["training_map_file"], "rb") as inp:
                training_map = pickle.load(inp)

        # Fix values
        kernel_map.direct_map = direct_map
        kernel_map.inverse_map = inverse_map
        kernel_map.mnd_index = mnd_index
        kernel_map.__trained = config["trained"]
        if config["trained"]:
            del kernel_map.__training_map
        else:
            kernel_map.__training_map = training_map

        return kernel_map
