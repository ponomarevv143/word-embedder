import json
import os
from typing import Optional, Union, List, Iterable, Tuple

import faiss.contrib.torch_utils
import numpy as np
import torch
from faiss import IndexFlatL2, IndexIVFFlat

from src.Exceptions import MNDIndexException


class MNDIndex:
    """
    MNDIndex flattens the tensor of M axis each containing N_i elements to a single П N_i dimensional vector,
    then stores it in the faiss [https://github.com/facebookresearch/faiss] index for addition and k-neighbor
    retrieval.
    """
    def __init__(self,
                 size: Optional[Union[Iterable[int], torch.Size]] = None,
                 initial_points: Optional[List[torch.Tensor]] = None,
                 n_cells: Optional[int] = 3,
                 n_probes: Optional[int] = 3):
        self.size = None
        self.index = None
        self.__built = False
        self.n_cells = n_cells if n_cells > 0 else 1
        self.n_probes = n_probes if (n_probes > 0) and (n_probes <= n_cells) else n_cells
        self.__build_size(size)
        if initial_points:
            self.build(initial_points)

    def build(self,
              initial_points: List[torch.Tensor]):
        """
        Build the index with passed initial points.
        """
        if not self.__built:
            self.__build_size(initial_points[0].size())
            self.__build_index()

        self.__check_size(initial_points)
        initial_points_t = torch.stack([self.flatten_map(point) for point in initial_points])
        self.index.train(initial_points_t)
        self.index.add(initial_points_t)
        self.index.make_direct_map()

    def __build_size(self, size: Optional[Union[Iterable[int], torch.Size]]):
        if size:
            if self.size is None:
                try:
                    for dim in size:
                        if dim < 0:
                            raise MNDIndexException("Received a size with a negative dimension.")
                except Exception:
                    raise MNDIndexException("Unknown exception has occurred while checking the dimensions for "
                                            "the passed size.")
                self.size = size
                self.flatten_map = lambda t: torch.flatten(t)
                self.unflatten_map = lambda t: torch.reshape(t, self.size)
                self.__build_index()
                self.__built = True

    def __build_index(self):
        dim = int(np.product(self.size))
        self.quantizer = IndexFlatL2(dim)
        self.index = IndexIVFFlat(self.quantizer, dim, self.n_cells)
        self.index.n_probes = self.n_probes

    def add(self, points: List[torch.Tensor]):
        """
        Adds new tensors
        """
        self.__check_size(points)
        points_transformed = torch.stack([self.flatten_map(point) for point in points])
        if not self.index.is_trained:
            self.index.train(points_transformed)
        self.index.add(points_transformed)
        self.index.make_direct_map()

    def __check_size(self, points: List[torch.Tensor]):
        """
        Checks if all the sizes confine to the predefined size. Throws MNDIndexException if not
        """
        for point in points:
            if list(point.size()) != self.size:
                raise MNDIndexException(f"Incorrect size has been passed to the index: "
                                        f"expected {self.size}, got {point.size()}")

    def __getitem__(self, query: Union[torch.Tensor,
                                       Tuple[torch.Tensor, int]]) -> List[int]:
        """
        Returns the indices of the closest k elements for a given tensor.
        :param query: Tuple of the form (T, k), where T is the tensor to searched, and k is the number of neighbors to
        pull. In case only one neighbor is required, tensor T may be passed directly as index[T] instead of
        index[(T, k)]
        :return: List of indices of closest neighbors.
        """
        return self.get_neighbors_indices(*query) if type(query) is tuple else self.get_neighbors_indices(query)

    def update(self, values: Union[Tuple[torch.Tensor, torch.Tensor],
                                   Iterable[Tuple[torch.Tensor, torch.Tensor]]]) -> None:
        """
        Given a pair or a collection of pairs of form (old_value, new_value), updates old value with
        the new value in the database.
        :param values: A pair or a collection of pairs of the form (old_value, new_value).
        """

        # self[values[0]] -> [idx], retrieve the index, replace Index[idx] <- new_value.
        # A) values[0] is in the form of [idx], translate it to the form Tensor([idx]) of shape (1, 1)
        # B) values[1] is a tensor of the shape [d1, d2, ...], flatten it and translate to the form [1, П d_i]
        # C) Update the values
        if type(values) is tuple:
            self.index.update_vectors(torch.tensor(self[values[0]], dtype=torch.int64),
                                      self.flatten_map(values[1]).reshape(1, -1))
        else:
            for value in values:
                self.update(value)

    def reconstruct(self, idx: int) -> torch.Tensor:
        """
        Returns the tensor associated with the passed idx.
        """
        return self.unflatten_map(self.index.reconstruct(idx))

    def get_neighbors_indices(self, query: torch.Tensor, k: int = 1) -> List[int]:
        """
        Returns the indices of the closest k elements for a given tensor.
        :param query: Tensor to get neighbors of.
        :param k: Number of neighbors.
        :return: List of indices of closest neighbors.
        """
        try:
            return self.index.search(self.flatten_map(query).reshape(1, -1), k)[1].tolist()[0]
        except Exception:
            raise MNDIndexException("Error has occurred while retrieving the query.")

    def get_neighbors(self, query: torch.Tensor, k: int = 1):
        """
        Get k neighbors close to the query tensor
        """
        if k < 1:
            raise MNDIndexException(f"Number of neighbors must be at least 1. Got {k}")
        # Get indices
        result_indices = self.get_neighbors_indices(query, k)
        # Map the indices to the tensors
        return [
            self.reconstruct(idx) for idx in result_indices if idx > -1
        ]

    def save(self,
             directory: str,
             conf_file: str = "mnd_index.json",
             index_file: str = "data.index") -> None:
        try:
            if not os.path.exists(directory):
                os.mkdir(directory)
            with open(directory + conf_file, "w") as out:
                out.write(json.dumps({
                    "size": self.size,
                    "n_cells": self.n_cells,
                    "n_probes": self.n_probes,
                    "index_file": directory + index_file
                }))
            faiss.write_index(self.index, directory + index_file)
        except Exception as e:
            raise MNDIndexException(f"Error has occurred when saving the index: {e}")

    @staticmethod
    def load(conf_file: str):
        try:
            with open(conf_file, "r") as inp:
                config = json.loads(inp.read())
            faiss_index = faiss.read_index(config["index_file"])
            mnd_index = MNDIndex(
                size=[int(dim) for dim in config["size"]],
                n_cells=int(config["n_cells"]),
                n_probes=int(config["n_probes"])
            )
            mnd_index.index = faiss_index
            # mnd_index.index.is_trained = True
            return mnd_index
        except Exception as e:
            raise MNDIndexException(f"Error has occurred when loading the index: {e}")
