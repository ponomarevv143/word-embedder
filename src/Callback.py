from typing import List, Union


class Callback:
    __slots__ = "state"

    def __init__(self):
        self.state = None

    def initialize_state(self, *args, **kwargs):
        pass

    def __call__(self, *args, **kwargs):
        return self.state


class TrackerCallback(Callback):
    def __init__(self,
                 container_ptr: List,
                 fields: Union[str, List[str]]):
        super().__init__()
        self.fields = fields if type(fields) is list else [fields]
        self.__container_ptr = container_ptr

    def initialize_state(self, *args, **kwargs):
        pass

    def __call__(self, *args, **kwargs):
        self.__container_ptr.append({field: val for field, val in kwargs.items() if field in self.fields})
