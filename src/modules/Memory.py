import numpy as np
import torch
from collections import deque
from collections import abc
from typing import Union, Iterable, Type, Callable, Any
from src.Exceptions import MemoryException, BroadcastingException
from src.utility import Broadcast


class Memory:
    """
    Memory module is responsible for storing the past signals and calculating
    the derivative operators.
    """
    def __init__(self,
                 dim: Union[int, Iterable[int], torch.Size],
                 size: int,
                 broadcast: Broadcast = Broadcast(),
                 default_initializer: Callable[[Iterable[int]], Any] = lambda dim: torch.rand(*dim),
                 default_type: Type = torch.Tensor):
        # Exception checking
        if not isinstance(size, int):
            try:
                size = int(size)
            except Exception:
                raise MemoryException(f"Size must be a non-zero integer. Got {type(size)}")
        if size < 1:
            raise MemoryException(f"Memory size must at least be 1. Got {size}")
        if not isinstance(broadcast, Broadcast):
            raise MemoryException(f"Broadcast argument must be of Broadcast type, got {type(broadcast)}")
        if default_type != broadcast.to_type:
            raise MemoryException(f"Passed default type is impossible to broadcast. Got {default_type}, "
                                  f"broadcastable type is {broadcast.to_type}")
        if type(default_initializer([0])) != default_type:
            raise MemoryException(f"Default initializer does not conform to the default type {default_type}")

        # Fields initialization
        self.size = size
        self.dim = [1, dim] if isinstance(dim, int) \
            else [*dim] if isinstance(dim, (abc.Iterable, torch.Size)) \
            else None
        for d in self.dim:
            if d < 1:
                raise MemoryException(f"Expected the dimensions to be at least 1. Received dimensions {self.dim}.")
        self.broadcast = broadcast
        self.default_initializer = default_initializer
        self.default_type = default_type

        # Initializing the storage and filling it with self.size tensors according to default_initializer
        self.__init_storage()
        self.clear()

    def __init_storage(self):
        """
        Checking for right size and creating the deque field
        """
        if self.dim is None:
            raise MemoryException("Failed to create a Memory unit. "
                                  "Expected dimension to be an integer, an iterable or torch.Size")
        if not isinstance(self.size, int):
            raise MemoryException(f"Failed to create a Memory unit. "
                                  f"Expected size to be an integer, got {type(self.size)}")
        self.storage = deque(maxlen=self.size)

    def push(self,
             x: Union[np.ndarray, list, torch.Tensor]):
        """
        Push element x to the memory using the inverse broadcast (default: to list)
        :param x: Element to be pushed.
        """
        if isinstance(x, np.ndarray):
            if list(x.shape) != self.dim:
                raise MemoryException(f"Expected the input to have dimension {self.dim}, got {x.shape}")
        if isinstance(x, torch.Tensor):
            if list(x.size()) != self.dim:
                raise MemoryException(f"Expected the input to have dimension {self.dim}, got {list(x.size())}")
        try:
            # Apply inverse transform to transform everything to the default type.
            self.storage.append(self.broadcast.inverse(x))
        except BroadcastingException:
            raise MemoryException(f"Cannot broadcast {type(x)} to list")

    def clear(self):
        for _ in range(self.size):
            self.push(self.default_initializer(self.dim))

    def __iter__(self):
        for x in self.storage:
            yield x

    def __getitem__(self,
                    item: Union[int, slice]):
        if isinstance(item, slice):
            indices = range(*item.indices(len(self.storage)))
            return self.broadcast([self.storage[i] for i in indices])
        elif isinstance(item, int):
            return self.broadcast(self.storage[item])
        else:
            raise MemoryException(f"Expected a slice or an integer, got {type(item)}")