from typing import Union, Iterable, List, Tuple

import numpy as np
import torch.nn

from src.modules import Memory

from src.Exceptions import *
from src.utility import Broadcast

from scipy.fft import dctn, idctn


class SequenceAnalysisUnit:
    """
    Module responsible for analysing the sequence of elements (tensors)
    and selecting the most important ones, as well as estimating the
    mean of these elements depending on their importance.
    """

    def __init__(self,
                 element_dim: Union[int, Iterable[int], torch.Size],
                 lookback_window: int,
                 frequency_percentile: float = 80,
                 element_percentile: float = 5,
                 loss_fn=torch.nn.CrossEntropyLoss()):
        # Initialize the memory unit
        try:
            self.memory = Memory(dim=element_dim,
                                 size=lookback_window,
                                 broadcast=Broadcast(
                                     map={
                                         torch.Tensor: lambda x: x.numpy(),
                                         np.ndarray: lambda x: x,
                                         list: np.asarray
                                     },
                                     to_type=np.ndarray
                                 ),
                                 default_type=np.ndarray,
                                 default_initializer=lambda dim: np.zeros(dim))
        except MemoryException as e:
            raise SequenceAnalysisUnitException("Couldn't initialize the memory: ", e)

        # Set the percentiles
        if (frequency_percentile > 100) or (frequency_percentile < 0):
            raise SequenceAnalysisUnitException("Frequency percentile must be between 0 and 100.")
        self.frequency_percentile = frequency_percentile

        if (element_percentile > 100) or (element_percentile < 0):
            raise SequenceAnalysisUnitException("Element percentile must be between 0 and 100.")
        self.element_percentile = element_percentile

        # Broadcast from torch to numpy and back
        self.broadcast = Broadcast(
            map={
                np.ndarray: lambda array: torch.from_numpy(array),
                torch.Tensor: lambda t: t.numpy()
            }
        )
        self.loss_fn = loss_fn

    def push(self,
             tensor_data: Union[torch.Tensor, List[torch.Tensor]]) -> None:
        if type(tensor_data) is torch.Tensor:
            try:
                self.memory.push(tensor_data)
            except MemoryException as e:
                raise SequenceAnalysisUnitException("Couldn't push the tensor to memory: ", e)
        else:
            for t in tensor_data:
                self.push(t)

    def reconstruct(self) -> Union[torch.Tensor, List[torch.Tensor]]:
        """
        From the current memory, reconstructs the list [T'_1, ..., T'_N]
        using the filtering in the frequency domain:
         [f1, ..., fN] = DCT([T_1, T_2, ..., T_N])
         [f'1, ..., f'N] = Filter([f1, ..., fN])
         [T'_1, ..., T'_N] = IDCT([f1, ..., fN])

        :return: List of reconstructed tensors.
        """

        # Get the frequencies (Shape Nx1xD)
        frequencies = dctn([*self.memory])
        # Find unimportant frequencies and eliminate them
        filter_indices = np.abs(frequencies) < np.percentile(np.abs(frequencies), self.frequency_percentile)
        frequencies[filter_indices] = 0
        return [self.broadcast(reconstructed_arr) for reconstructed_arr in idctn(frequencies)]

    def estimate(self,
                 data: Union[torch.Tensor, List[torch.Tensor]],
                 calculate_mean: bool = False,
                 #                             First two are base case, second two are wrapper case
                 weights_offset: int = 0.1) -> Union[Union[Tuple[List[int], List[float]],
                                                           Tuple[List[int], List[float], torch.Tensor]],
                                                     List[Union[Tuple[List[int], List[float]],
                                                                Tuple[List[int], List[float], torch.Tensor]]]]:
        """
        Given the passed tensor list, estimates the loss of reconstruction and returns
        the indices and weights (normalized so that the sum of weights is 1) of all
        important elements of the tensor list

        :param data: Input data.
        :param calculate_mean: If true, also returns the mean calculated using the indices and weights.
        :param weights_offset: During the weights' estimation, used to make the maximal reconstruction loss of the
        filtered losses to be non-zero. The bigger this parameter is, the more uniform weights will be.
        :return: List of indices and their respective weights if calculate_mean is False, otherwise additionally
        returns the mean as a third element of the tuple.
        """
        # Push -> Reconstruct -> Estimate loss -> Find indices -> Repeat
        if type(data) is torch.Tensor:
            self.push(data)
            # Broadcast np.ndarray to torch.Tensor of the shape [lookback_window, 1, embedding_dim]
            true_tensors = self.broadcast(self.memory[:])
            reconstructed_tensors = self.reconstruct()
            # Reconstruct the tensors and find the reconstruction losses
            losses = [self.loss_fn(reconstructed_t, true_t).item()
                      for reconstructed_t, true_t
                      in zip(reconstructed_tensors, true_tensors)]

            # Calculate the loss threshold and find the indices of elements for which the loss is in the lower bracket
            loss_threshold = np.percentile(losses, self.element_percentile)
            filtered_indices = [i for i in range(self.memory.size) if losses[i] <= loss_threshold]

            # Estimate weights assigning the more weight the less loss the element has
            # For that, estimate the maximal loss and subtract from it all the losses.
            # Maximal loss would turn to 0
            weights = [losses[i] for i in filtered_indices]
            weights = [(max(weights) - w + weights_offset) for w in weights] if len(weights) > 1 else [1]
            weights = [w / sum(weights) for w in weights]
            if calculate_mean:
                mean = self.broadcast(sum(weights[i] * self.memory[filtered_indices[i]] for i in range(len(filtered_indices))))
            return (filtered_indices, weights, mean) if calculate_mean else (filtered_indices, weights)
        else:
            # Wrapper case
            return [self.estimate(data=t, calculate_mean=calculate_mean, weights_offset=weights_offset) for t in data]
