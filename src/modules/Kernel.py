import os
from typing import Dict, Iterable, Union

import torch

from src.Exceptions import KernelException, KernelMapException
from src.utility import KernelMap
import json


class Kernel:
    """
    Kernel is responsible for building and sustaining the word-lattice,
    providing an interface for retrieving their kernel representations
    and training on the words.
    """
    def __init__(self,
                 embedding_dimension: int,
                 lr: float = 1e-3,
                 training_subset_length: int = 130,
                 retraining_frequency: int = 1000,
                 n_cells: int = 5,
                 n_probes: int = 3):
        if embedding_dimension < 0:
            raise KernelException(f"Embedding dimension must be positive, got {embedding_dimension}.")
        if lr < 0:
            raise KernelException(f"Learning rate must be positive, got {lr}.")
        self.size = embedding_dimension
        self.lr = lr
        self.dictionary = KernelMap(embedding_dimension,
                                    training_subset_length=training_subset_length,
                                    retraining_frequency=retraining_frequency,
                                    n_cells=n_cells,
                                    n_probes=n_probes)

    def __getitem__(self,
                    word: str) -> torch.Tensor:
        return self.get(word)

    def get(self, word: str) -> torch.Tensor:
        """
        Facade for KernelMap.get() -> Forward search (pass word, get its representation)
        """
        try:
            return self.dictionary.get(word)
        except KernelMapException:
            raise KernelException(f"Key '{word}' is not found in the kernel map.")

    def search(self, representation: torch.Tensor) -> str:
        """
        Facade for KernelMap.get() -> Inverse search (pass representation, get closest word)
        """
        try:
            return self.dictionary.search(representation)
        except KernelMapException as e:
            raise KernelException(f"Exception has occurred during the reverse-search of the representation: {e}")

    def push(self,
             data: Union[str, Iterable[str]]) -> None:
        """
        Pushes the word in the dictionary if it's not present initializing it
        randomly as a d x d matrix. If it's present, does nothing.

        :param data: Word or a collection of words to be pushed.
        """
        if type(data) is str:
            if not self.dictionary.key_exists(word=data):
                self.dictionary.set(word=data, representation=torch.rand(self.size, self.size))
        else:
            for word in data:
                self.push(word)

    def train(self,
              key: str,
              target: torch.Tensor,
              context: torch.Tensor) -> None:
        """
        Moves the tensor corresponding to the key to the target tensor.

        :param key: The word to train.
        :param target: Target tensor of dimensionality D^2 conforming to the embedding dimension.
        :param context: The context vector of dimensionality 1xD conforming to the embedding dimension.
        :raises KernelException: If the target tensor or the context vectors are of the wrong size.
        """
        if [*target.size()] != [self.size, self.size]:
            raise KernelException(f"Target tensor must be of size {[self.size, self.size]}, "
                                  f"received {[*target.size()]}.")
        if [*context.size()] != [1, self.size]:
            raise KernelException(f"Context vector must be of size {[1, self.size]}, "
                                  f"received {[*context.size()]}.")

        # If the word isn't present in the dictionary, add it.
        self.push(key)

        # Calculate the gradient as sum_k r_k * (F'_k - F_k)
        # where r_k is the k-th element of the context vector,
        # F'_k is the k-th row of the target tensor
        # and F_k is the k-th row of the tensor corresponding to the passed word
        old_F = self[key]
        grad_F = torch.mul(context.T,
                           (target - old_F))
        new_F = old_F + grad_F
        self.dictionary.set(key, new_F)

    def save(self,
             directory: str,
             conf_file: str = "kernel.json") -> None:
        if not os.path.exists(directory):
            os.mkdir(directory)

        # Save kernel map
        self.dictionary.save(directory=directory + "kernel_map/")

        # Save config
        with open(directory + conf_file, "w") as out:
            out.write(json.dumps(
                {
                    "embedding_dimension": self.size,
                    "lr": self.lr,
                    "kernel_map_conf_file": directory + "kernel_map/" + "kernel_map.json"
                }
            ))

    @staticmethod
    def load(conf_file: str):
        with open(conf_file, "r") as inp:
            config = json.loads(inp.read())

        dictionary = KernelMap.load(config["kernel_map_conf_file"])
        kernel = Kernel(embedding_dimension=int(config["embedding_dimension"]),
                        lr=float(config["lr"]))
        kernel.dictionary = dictionary

        return kernel
