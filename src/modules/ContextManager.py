import json
import os
from typing import Union, Optional, Dict, Any, Callable

import torch

from src.Exceptions import ContextManagerException


class ContextManager:
    """
    Facade responsible for managing the context vector.
    """
    def __init__(self,
                 context_size: Union[int, torch.Size],
                 net: Optional[torch.nn.Module] = None,
                 optim_class=torch.optim.Adam,
                 optim_params: Dict[str, Any] = {"lr": 1e-3},
                 loss_fn=torch.nn.CrossEntropyLoss()
                 ):

        # Context size - the return size of the context vector. Has the form of [1, d_out].
        self.size = [*context_size] if type(context_size) is torch.Size \
            else [1, context_size] if (type(context_size) is int) and (context_size > 0) \
            else None
        if self.size is None:
            raise ContextManagerException("Expected context size to be torch.Size or a positive int, got "
                                          f"{context_size}")

        # Predefined net. If None, initializes it with a simple fully-connected layer and a softmax after it.
        if net:
            self.check_net(net)
        self.net = net if net \
            else torch.nn.Sequential(
                torch.nn.Linear(in_features=self.size[-1], out_features=self.size[-1]),
                torch.nn.Softmax()
            )

        # Optimizer and Loss functions
        self.optimizer = optim_class(self.net.parameters(), **optim_params)
        self.loss_fn = loss_fn

        # Initialization complete
        self.__initialized = True

        # Signal is the previous value of the context vector
        self.signal = None
        self.__build()

    def check_net(self,
                  net: torch.nn.Module) -> None:
        """
        Checks the predefined net.
        The net must be able to take the vector of the size [1, d_in] (signal size) and return
        the context vector of the size [1, d_out] that is positive-definite and sums up to 1.

        :param net: Predefined net.
        :raises ContextManagerException: if the net is not compatible with the inputs, outputs, there's an error
        in passing the signal forward, the result is not positive-definite or the result doesn't sum up to 1.
        """
        try:
            result = net(torch.rand(self.size))
            assert self.size[1:] == [*result.size()][1:]
            assert torch.all(result > 0)
            assert abs(torch.sum(result) - 1) < 1e-3
        except Exception:
            raise ContextManagerException("Net incompatible with the required architecture. "
                                          f"Must be able to take and return vectors of size {self.size}] "
                                          f"that are positive-definite and sum to 1.")

    def check_size_compatible(self,
                              tensor: torch.Tensor) -> bool:
        """
        Checks whether the tensor is compatible with the input (signal) size.

        :param tensor: Specified tensor.
        :return: True if the sizes are compatible, False otherwise.
        """
        return [*tensor.size()][1:] == self.size[1:]

    def __build(self) -> None:
        self.signal = torch.zeros(self.size, requires_grad=True)

    def __call__(self,
                 *args,
                 **kwargs) -> torch.Tensor:
        """
        Passes the signal through the net to get the context vector.

        :param signal: The input signal.
        :return: The context vector.
        :raises ContextManagerException: If there was an error during forwarding the signal through the net, or if
        there's a mismatch in the input size and the signal size.
        """
        try:
            new_signal = self.net(self.signal)
            self.push(new_signal)
            return new_signal
        except Exception:
            raise ContextManagerException("Error has occurred when passing the signal through the net.")

    def push(self,
             new_signal: torch.Tensor):
        if self.check_size_compatible(new_signal):
            self.signal = new_signal.detach()
        else:
            raise ContextManagerException(f"Signal size incompatible: expected {self.size}, "
                                          f"received {[*new_signal.size()]}.")

    def train(self,
              true_context: torch.Tensor) -> None:
        """
        Train the net as L(true_context, net(previous_context)) -> min {net.parameters()}

        :param true_context: Context vector to train the net.
        :raises ContextManagerException: If the passed vector does not confine to the predefined context size,
        or if there has been an error during the forward propagation.
        """
        if self.check_size_compatible(true_context):
            self.net.zero_grad()
            predicted_context = self()
            loss = self.loss_fn(true_context, predicted_context)
            loss.backward(retain_graph=True)
            self.optimizer.step()
            self.push(new_signal=true_context)
        else:
            raise ContextManagerException(f"Passed context failed the size check. "
                                          f"Expected size {self.size}, received {[*true_context.size()]}.")

    def save(self,
             directory: str,
             conf_file: str = "context_manager.json") -> None:
        if not os.path.exists(directory):
            os.mkdir(directory)
        torch.save(self.net, directory + "net.pt")
        with open(directory + conf_file, "w") as out:
            out.write(json.dumps(
                {
                    "size": self.size,
                    "net_file": directory + "net.pt",
                }
            ))

    @staticmethod
    def load(conf_file: str):
        with open(conf_file, "r") as inp:
            config = json.loads(inp.read())
        net = torch.load(config["net_file"])
        return ContextManager(
            context_size=torch.zeros([int(dim) for dim in config["size"]]).size(),
            net=net
        )