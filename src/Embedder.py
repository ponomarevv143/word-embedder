from typing import Optional, Dict, Any

import torch

from Callback import Callback
from src.Exceptions import *
from src.modules import *

import json


class Embedder:
    def __init__(self,
                 embedding_dimension: int,
                 lookback_window: int,
                 kernel_lr: float = 1e-3,
                 kernel_n_cells: int = 5,
                 kernel_n_probes: int = 3,
                 kernel_training_subset_length: int = 120,
                 kernel_retraining_frequency: int = 1000,
                 context_net: Optional[torch.nn.Module] = None,
                 context_optim_class=torch.optim.Adam,
                 context_optim_params: Dict[str, Any] = {"lr": 1e-3},
                 context_loss_fn=torch.nn.CrossEntropyLoss(),
                 sau_frequency_percentile: float = 80,
                 sau_element_percentile: float = 5,
                 sau_loss_fn=torch.nn.CrossEntropyLoss(),
                 base_word: str = " "):
        self.embedding_dim = embedding_dimension
        # Initialize the kernel
        try:
            self.kernel = Kernel(
                embedding_dimension=embedding_dimension,
                lr=kernel_lr,
                training_subset_length=kernel_training_subset_length,
                retraining_frequency=kernel_retraining_frequency,
                n_cells=kernel_n_cells,
                n_probes=kernel_n_probes
            )
        except KernelException as e:
            raise EmbedderException("Problem has occurred during the initialization of the kernel. ", e)

        # Initialize the context manager
        try:
            self.context_manager = ContextManager(
                context_size=embedding_dimension,
                net=context_net,
                optim_class=context_optim_class,
                optim_params=context_optim_params,
                loss_fn=context_loss_fn
            )
        except ContextManagerException as e:
            raise EmbedderException("Problem has occurred during the initialization of the context manager. ", e)

        # Initialize SAU
        try:
            self.sau = SequenceAnalysisUnit(
                element_dim=embedding_dimension,
                lookback_window=lookback_window,
                frequency_percentile=sau_frequency_percentile,
                element_percentile=sau_element_percentile,
                loss_fn=sau_loss_fn
            )
        except SequenceAnalysisUnitException as e:
            raise EmbedderException("Problem has occurred during the initialization of the sequence analysis unit. ", e)

        self.lookback_window = lookback_window
        self.__word_deque = deque(maxlen=lookback_window)
        # For stability, fill the deque and add the base word to the dictionary.
        self.kernel.push(base_word)
        for _ in range(lookback_window):
            self.__word_deque.append(base_word)

    def estimate(self,
                 data: Union[str, List[str]],
                 train: bool = False,
                 callbacks: Optional[List[Callback]] = None) -> Union[Tuple[torch.Tensor, torch.Tensor],
                                                                List[Tuple[torch.Tensor, torch.Tensor]]]:
        """
        Estimation of representation-context pairs given the word or a list of words.

        :param data: Word or the list of words.
        :param train: Whether to train the embedder or not.
        :return: Tuple (representation, context) or the list of such tuples respectively.
        """

        # Base case: one word
        if isinstance(data, str):
            # 1. Try pushing to the kernel, retrieve the current tensor representation
            self.kernel.push(data)
            F = self.kernel[data]

            # 2. Get the predicted context r(t)
            context = self.context_manager()
            # 3. Push context to the SAU, estimate new indices and weights
            indices, weights = self.sau.estimate(context)

            # 4. Estimate the mean kernel as sum p_i F[w_j_i] (J is the intermediary index 'indices')
            F_mean = sum([weights[i] * self.kernel[self.__word_deque[indices[i]]] for i in range(len(indices))])

            # 5. Train the kernel on the mean kernel given the current context
            if train:
                self.kernel.train(key=data, target=F_mean, context=context)

            # TODO: figure out lr
            lr = 1e-1

            # 6. Estimate the new context r(t+1) = r(t) * (F_mean^[-1] * F[w]), train the manager
            new_context = torch.matmul(context, lr * torch.matmul(
                F_mean, torch.inverse(F)
            ) + (1-lr) * torch.eye(n=self.embedding_dim, m=self.embedding_dim)).detach()
            if train:
                self.context_manager.train(new_context)
            else:
                self.context_manager.push(new_context)

            # 7. Push the word to the deque
            self.__word_deque.append(data)

            if callbacks:
                for callback in callbacks:
                    callback.initialize_state(
                        data=data,
                        context=context,
                        indices=indices,
                        weights=weights,
                        F_mean=F_mean,
                        new_context=new_context,
                        representation=torch.matmul(context, F)
                    )
                    callback(
                        data=data,
                        context=context,
                        indices=indices,
                        weights=weights,
                        F_mean=F_mean,
                        new_context=new_context,
                        representation=torch.matmul(context, F)
                    )

            # Finally, estimate the representation and return (rep, context)
            return torch.matmul(context, F), context
        else:
            # Push all words preemptively
            self.kernel.push(data)
            # Wrapper for the base case
            return [self.estimate(data=word, train=train, callbacks=callbacks) for word in data]

    def get_kernel(self, word: str) -> torch.Tensor:
        try:
            return self.kernel.get(word)
        except KernelException as e:
            raise EmbedderException(f"Error has occurred during the kernel retrieval: {e}")

    def search_word(self, kernel: torch.Tensor) -> str:
        try:
            return self.kernel.search(kernel)
        except KernelException as e:
            raise EmbedderException(f"Error has occurred during reverse search: {e}")

    def save(self,
             directory: str = "embedder/",
             conf_file: str = "embedder.json"):
        if not os.path.exists(directory):
            os.mkdir(directory)
        self.kernel.save(directory=directory + "kernel/")
        self.context_manager.save(directory=directory + "context_manager/")
        with open(directory + conf_file, "w") as out:
            out.write(json.dumps(
                {
                    "embedding_dim": self.embedding_dim,
                    "lookback_window": self.lookback_window,
                    "kernel_conf_file": directory + "kernel/" + "kernel.json",
                    "context_manager_conf_file": directory + "context_manager/" + "context_manager.json"
                }
            ))

    @staticmethod
    def load(conf_file="./embedder/embedder.json"):
        with open(conf_file, "r") as inp:
            config = json.loads(inp.read())

        embedder = Embedder(
            embedding_dimension=int(config["embedding_dim"]),
            lookback_window=int(config["lookback_window"])
        )
        embedder.kernel = Kernel.load(config["kernel_conf_file"])
        embedder.context_manager = ContextManager.load(config["context_manager_conf_file"])
        return embedder
