import unittest
import torch
from src.modules import *
from src.Exceptions import *


class MemoryTestCase(unittest.TestCase):
    def test_initialization(self):
        Memory(1, 1)
        pass

    def test_dimensions(self):
        Memory(3, 1)
        Memory([3, 3], 1)
        Memory((3, 3), 1)
        Memory(set([3, 3]), 1)
        Memory(torch.rand(1, 2).size(), 1)

    def test_sizes(self):
        self.assertRaises(MemoryException, lambda: Memory(3, -1))
        self.assertRaises(MemoryException, lambda: Memory(3, 0))
        Memory(3, 1.0)
        Memory(3, 10)

    def test_push(self):
        m = Memory(3, 1)
        x = torch.rand(1, 3)
        m.push(x)

    def test_getitem(self):
        m = Memory(3, 3)
        x = torch.rand(1, 3)
        y = torch.rand(1, 3)
        m.push(x)
        m.push(y)
        self.assertTrue(torch.equal(m[-2:], torch.stack([x, y])))


class KernelTestCase(unittest.TestCase):
    def test_initialization(self):
        Kernel(3)
        with self.assertRaises(KernelException):
            Kernel(-1)
        with self.assertRaises(KernelException):
            Kernel(embedding_dimension=3, lr=-1)
        pass

    def test_push(self):
        word = 'word'
        dimension = 3
        kernel = Kernel(dimension)
        with self.assertRaises(KernelException):
            kernel[word]
        kernel.push(word)
        self.assertTrue([*kernel[word].size()] == [dimension, dimension])

    def test_train(self):
        word = 'word'
        dimension = 3
        kernel = Kernel(dimension)

        target_tensor = torch.Tensor([[1, 2, 3],
                                      [4, 5, 6],
                                      [7, 8, 9]])
        target_context = torch.Tensor([[10, 20, 30]])

        kernel.train(word,
                     target=target_tensor,
                     context=target_context)

    def test_search(self):
        dimension = 3
        num_bruteforce = 10
        training_subset_length = 120
        num_index_search = 1e3

        target_context = torch.Tensor([[1, 1, 1]])

        accuracy_margin = 0.75

        for num in [num_bruteforce, num_index_search]:
            num = int(num)
            words = [f"word_{i}" for i in range(num)]
            targets = [torch.rand(dimension, dimension) for _ in range(num)]
            kernel = Kernel(dimension,
                            training_subset_length=training_subset_length)
            for word, target in zip(words, targets):
                kernel.train(word,
                             target,
                             context=target_context)
            counter = 0
            for i in range(num):
                counter += 1 if words[i] == kernel.search(targets[i]) else 0
            print(f"# of elements: {num}\nAccuracy: {counter/num}")
            self.assertTrue(counter/num > accuracy_margin)


class ContextManagerTestCase(unittest.TestCase):
    def test_initialization(self):
        ContextManager(100)
        temp = torch.rand(1, 5)
        ContextManager(temp.size())
        pass

    def test_check_size(self):
        manager = ContextManager(torch.rand(1, 5).size())
        manager.check_size_compatible(tensor=torch.rand(1, 5))
        manager.check_size_compatible(tensor=torch.rand(3, 5))
        self.assertFalse(manager.check_size_compatible(tensor=torch.rand(1, 4)))
        pass

    def test_check_net(self):
        manager = ContextManager(5)
        # Default net should pass the test
        manager.check_net(manager.net)

        # Incorrect output size
        with self.assertRaises(ContextManagerException):
            net = torch.nn.Sequential(
                torch.nn.Linear(in_features=5, out_features=10),
                torch.nn.Softmax()
            )
            manager.check_net(net)

        # Incorrect input size
        with self.assertRaises(ContextManagerException):
            net = torch.nn.Sequential(
                torch.nn.Linear(in_features=3, out_features=5),
                torch.nn.Softmax()
            )
            manager.check_net(net)

        # No normalization
        with self.assertRaises(ContextManagerException):
            net = torch.nn.Sequential(
                torch.nn.Linear(in_features=5, out_features=5)
            )
            manager.check_net(net)

        # Custom net that should pass
        net = torch.nn.Sequential(
            torch.nn.Linear(in_features=5, out_features=10),
            torch.nn.Linear(in_features=10, out_features=5),
            torch.nn.Softmax()
        )
        manager.check_net(net)
        pass

    def test_call(self):
        manager = ContextManager(5)
        # Get first context
        context = manager()
        # First context must be the predecessor to the new context
        self.assertTrue(torch.equal(context, manager.signal))
        # Get new context
        new_context = manager()
        # Contexts must differ, even if slightly
        self.assertFalse(torch.equal(context, new_context))
        pass

    def test_train(self):
        manager = ContextManager(5)
        true_context = torch.rand(1, 5)
        manager.train(true_context)
        pass


class SequenceAnalysisUnitTestCase(unittest.TestCase):
    def test_initialization(self):
        # Right initialization
        SequenceAnalysisUnit(element_dim=3, lookback_window=10)
        # Error: negative dimension
        with self.assertRaises(SequenceAnalysisUnitException):
            SequenceAnalysisUnit(element_dim=-1, lookback_window=10)
        # Error: negative lookback window
        with self.assertRaises(SequenceAnalysisUnitException):
            SequenceAnalysisUnit(element_dim=1, lookback_window=-10)

    def test_reconstruct(self):
        # Test parameters
        element_dim = 3
        lookback_window = 10
        sau = SequenceAnalysisUnit(element_dim=element_dim, lookback_window=lookback_window)

        # Input data
        tensors = torch.randn(lookback_window, 1, element_dim)
        tensors = (tensors - tensors.min()) / (tensors.max() - tensors.min())
        tensors = torch.softmax(tensors, dim=-1).view(-1, element_dim)
        tensor_list = torch.split(tensors, 1)
        sau.push(tensor_list)

        # Reconstruct
        result = sau.reconstruct()
        pass

    def test_estimate(self):
        # Test parameters
        element_dim = 3
        lookback_window = 100
        sau = SequenceAnalysisUnit(element_dim=element_dim, lookback_window=lookback_window)

        # Input data
        tensors = torch.randn(lookback_window, 1, element_dim)
        tensors = (tensors - tensors.min()) / (tensors.max() - tensors.min())
        tensors = torch.softmax(tensors, dim=-1).view(-1, element_dim)
        tensor_list = torch.split(tensors, 1)

        result = sau.estimate(data=tensor_list,
                              calculate_mean=True)
        for idx, weights, mean in result:
            self.assertTrue(len(idx) > 0)
            self.assertTrue(abs(sum(weights) - 1) < 1e-3)
            self.assertEqual([*tensor_list[0].size()], [*mean.size()])


if __name__ == '__main__':
    unittest.main()
