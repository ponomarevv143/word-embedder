import unittest
import numpy as np
import torch
from src.utility import *
from src.Exceptions import *


class BroadcastTestCase(unittest.TestCase):
    def test_default_init(self):
        self.assertEqual(len(Broadcast().map), 3)

    def test_default_broadcast(self):
        b = Broadcast()
        temp = np.random.rand(100, 200, 300)
        self.assertTrue(np.array_equal(b(temp).numpy(), temp))
        self.assertTrue(np.linalg.norm(b(temp.tolist()).numpy() - temp) < 1e-3)

    def test_default_from_types(self):
        b = Broadcast()
        types = list(b)
        self.assertEqual(types, [np.ndarray, list, torch.Tensor])

    def test_inverse(self):
        b = Broadcast()
        temp = np.random.rand(100, 200, 300)
        self.assertEqual(temp.tolist(), b.inverse(temp))
        self.assertEqual(temp.tolist(), b.inverse(torch.from_numpy(temp)))

    def test_exceptions(self):
        self.assertRaises(BroadcastingException, lambda: Broadcast(map=list()))
        self.assertRaises(BroadcastingException, lambda: Broadcast()(tuple([1, 2, 3])))
        self.assertRaises(BroadcastingException, lambda: Broadcast().inverse(tuple([1, 2, 3])))


class MNDIndexTestCase(unittest.TestCase):
    def test_initialization(self):
        # Initialize with an int
        index = MNDIndex([5])
        self.assertEqual([5], index.size)

        # Initialize with an iterable
        index = MNDIndex([3, 15, 1])
        self.assertEqual([3, 15, 1], index.size)

        # Initialize with torch.Size
        temp = torch.rand(2, 3, 5)
        index = MNDIndex(temp.size())
        self.assertEqual(temp.size(), index.size)

        # Assert fails
        with self.assertRaises(MNDIndexException):
            index = MNDIndex([-1])

        with self.assertRaises(MNDIndexException):
            index = MNDIndex([5, -1, 3])

        # Size must be iterable, not a single int
        with self.assertRaises(MNDIndexException):
            index = MNDIndex(6)

    def test_add(self):
        size = [6, 8]
        num = 120

        data = [torch.rand(size) for _ in range(num)]
        index = MNDIndex(size)
        index.add(data)

        # Assert reflexivity for the data - Index.ClosestNeighbor[point] = point for all points in data
        # Account for the fact .get_neighbors returns a list with a single element
        for point in data:
            self.assertTrue(torch.equal(point, index.get_neighbors(point)[0]))

    def test_update(self):
        size = [6, 8]
        num = 120
        lr = 1e-3
        accuracy_margin = 0.75

        data = [torch.rand(size) for _ in range(num)]
        index = MNDIndex(size)
        index.add(data)

        # Add a small random noise to simulate gradual movement of the elements
        new_data = [point + lr * torch.rand(size) for point in data]
        vals = list(zip(data, new_data))
        index.update(vals)

        # Assert the new points are more-or-less in the same cells as the previous ones
        counter = 0
        for i in range(num):
            try:
                if torch.equal(index.get_neighbors(data[i])[0], new_data[i]):
                    counter += 1
            except IndexError:
                pass

        print(f"Accuracy: {counter/num}")
        self.assertTrue(counter / num >= accuracy_margin)


class KernelMapTestCase(unittest.TestCase):
    def test_initialization(self):
        kernel_map = KernelMap(3, training_subset_length=10)
        self.assertFalse(kernel_map.mnd_index.index.is_trained)

    def test_set_pretrain(self):
        dim = 3
        num = 1000
        accuracy_margin = 0.75
        print_prediction = False

        kernel_map = KernelMap(dim, training_subset_length=num)
        kernel_factory = lambda: torch.rand(dim, dim)

        kernels = [kernel_factory() for _ in range(num)]
        for i in range(num):
            kernel_map.set(f"test_{i}", kernels[i])
        self.assertFalse(kernel_map.direct_map)
        self.assertFalse(kernel_map.inverse_map)

        counter = 0
        for i in range(num):
            rep_found = kernel_map.get(word=f"test_{i}")
            counter += 1 if torch.equal(rep_found, kernels[i]) else 0
            # if print_prediction:
            #     print(f"Found representation: {rep_found} | True representation: {kernels[i]}")
        print(f"Forward Search Accuracy: {counter / num}")
        self.assertTrue(counter / num > accuracy_margin)

        counter = 0
        for i in range(num):
            word_found = kernel_map.search(kernels[i])
            counter += 1 if word_found == f"test_{i}" else 0
            if print_prediction:
                print(f"Found word: {word_found} | True word: test_{i}")
        print(f"Inverse Search Accuracy: {counter/num}")
        self.assertTrue(counter/num > accuracy_margin)

    def test_train(self):
        dim = 3
        training_num = 120
        num = 500
        accuracy_margin = 0.75
        print_prediction = False

        kernel_map = KernelMap(dim, training_subset_length=training_num)
        kernel_factory = lambda: torch.rand(dim, dim)

        # Fill up the training dict
        kernels = [kernel_factory() for _ in range(training_num)]
        for i in range(training_num):
            kernel_map.set(f"test_{i}", kernels[i])
        self.assertFalse(kernel_map.direct_map)
        self.assertFalse(kernel_map.inverse_map)

        # Add one more element to begin training
        kernels.append(kernel_factory())
        kernel_map.set(f"test_{training_num}", kernels[-1])

        self.assertTrue(kernel_map.direct_map)
        self.assertTrue(kernel_map.inverse_map)

        self.assertEqual(len(kernel_map.direct_map), training_num + 1)
        self.assertEqual(len(kernel_map.inverse_map), training_num + 1)

        # Add the remaining elements
        words = [f"test_{i}" for i in range(training_num + 1, num)]
        new_kernels = [kernel_factory() for _ in range(training_num + 1, num)]
        kernels += new_kernels
        # Sanity check
        self.assertEqual(len(kernels), num)
        # Add all elements, assert all elements have been added
        kernel_map.set(words, new_kernels)
        self.assertEqual(len(kernel_map.direct_map), num)
        self.assertEqual(len(kernel_map.inverse_map), num)
        self.assertTrue(kernel_map.mnd_index.index.is_trained)

        counter = 0
        for i in range(num):
            rep_found = kernel_map.get(word=f"test_{i}")
            counter += 1 if torch.equal(rep_found, kernels[i]) else 0
            # if print_prediction:
            #     print(f"Found representation: {rep_found} | True representation: {kernels[i]}")
        print(f"Forward Search Accuracy: {counter / num}")
        self.assertTrue(counter / num > accuracy_margin)

        counter = 0
        for i in range(num):
            word_found = kernel_map.search(kernels[i])
            counter += 1 if word_found == f"test_{i}" else 0
            if print_prediction:
                print(f"Found word: {word_found} | True word: test_{i}")
        print(f"Inverse Search Accuracy: {counter/num}")
        self.assertTrue(counter/num > accuracy_margin)


if __name__ == '__main__':
    unittest.main()
