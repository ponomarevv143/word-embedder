import unittest
import torch
from src import Embedder
from src.Exceptions import *


class EmbedderTestCase(unittest.TestCase):
    def test_initialization(self):
        # Test sizes
        Embedder(10, 5)
        with self.assertRaises(EmbedderException):
            Embedder(-1, 5)
        with self.assertRaises(EmbedderException):
            Embedder(10, -1)

        # Assert base word is initialized
        embedder = Embedder(10, 5)
        f, r = embedder.estimate(" ")
        pass

    def test_estimate(self):
        embedder = Embedder(2, 3)
        words = ["one", "two", "three", "four", "five", "six"]
        result = embedder.estimate(words)
        for f, r in result:
            print(f"Representation: {f.detach().numpy()[0]}\n"
                  f"Context: {r.detach().numpy()[0]}\n")
        pass

    def test_train(self):
        embedder = Embedder(3, 5)
        words = ["one", "two", "three", "four", "five", "six"] * 5
        result = embedder.estimate(words, train=True)
        for i, (f, r) in enumerate(result):
            print(f"Word: {words[i]}\n"
                  f"Representation: {f.detach().numpy()[0]}\n"
                  f"Context: {r.detach().numpy()[0]}\n")
        pass

    def test_serialization(self):
        embedder = Embedder(3, 5)
        words = [f"word_{i}" for i in range(30)]
        result = embedder.estimate(words, train=True)

        embedder.save()
        embedder = Embedder.load()
        result = embedder.estimate(words)
        for i, (f, r) in enumerate(result):
            print(f"Word: {words[i]}\n"
                  f"Representation: {f.detach().numpy()[0]}\n"
                  f"Context: {r.detach().numpy()[0]}\n")
        pass